extends Node2D

const blue_enemy = preload('res://assets/enemy/blue_enemy.tres')
const red_enemy = preload('res://assets/enemy/red_enemy.tres')
const green_enemy = preload('res://assets/enemy/green_enemy.tres')

var enemy = preload('res://scenes/Enemy.tscn')

func _on_EnemySpawn1Timer_timeout():
	var enemy_1 = enemy.instance()
	enemy_1.position = $Spawn1.position
	enemy_1.is_dead = false
	enemy_1.color = random()
	if enemy_1.color == 1:
		enemy_1.get_node('Sprite').set_sprite_frames(blue_enemy)
	elif enemy_1.color == 2:
		enemy_1.get_node('Sprite').set_sprite_frames(red_enemy)
	else:
		enemy_1.get_node('Sprite').set_sprite_frames(green_enemy)
	enemy_1.get_node('Sprite').flip_h = false
	add_child(enemy_1)

func random():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	return rng.randi_range(1, 3)


func _on_Right_Wall_body_entered(body):
	if body.name == "Bullet":
		body.disappear()


func _on_Top_Wall_body_entered(body):
	if body.name == "Bullet":
		body.disappear()

func _on_Left_Wall_body_entered(body):
	if body.name == "Bullet":
		body.disappear()


func _on_EnemySpawn2Timer_timeout():
	var enemy_2 = enemy.instance()
	enemy_2.position = $Spawn2.position
	enemy_2.is_dead = false
	enemy_2.color = random()
	if enemy_2.color == 1:
		enemy_2.get_node('Sprite').set_sprite_frames(blue_enemy)
	elif enemy_2.color == 2:
		enemy_2.get_node('Sprite').set_sprite_frames(red_enemy)
	else:
		enemy_2.get_node('Sprite').set_sprite_frames(green_enemy)
	enemy_2.get_node('Sprite').flip_h = true
	add_child(enemy_2)
