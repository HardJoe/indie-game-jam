extends KinematicBody2D

const speed = 100


var velocity = Vector2()
var is_dead = false
var color

func _physics_process(delta):
	if is_dead == false:
		var direction = (get_parent().get_node("Player").position - position).normalized()
		var motion = direction * speed * delta
		position += motion
		

func die():
	is_dead = true
	global.score += 1
	velocity = Vector2(0, 0)
	$Sprite.play("mati")
	$CollisionShape2D.set_deferred("disabled", true)
	$Timer.start()


func _on_Enemy_body_entered(body):
	if body.name == "Bullet":
		die()


func _on_Area2D_body_entered(body):
	if body.name == "Bullet":
		if color == 1:
			if get_parent().get_node("Player").color == 'g':
				die()
		elif color == 2:
			if get_parent().get_node("Player").color == 'b':
				die()
		else:
			if get_parent().get_node("Player").color == 'r':
				die()
		body.disappear()
	elif body.name == "Player":
		global.lives -= 1
		if (global.lives == 0):
			get_tree().change_scene(str("res://scenes/Game Over.tscn"))
			global.lives = 3
			global.score = 0


func _on_Timer_timeout():
	queue_free()
