extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 4000
export (int) var jump_speed = -500

const UP = Vector2(0,-1)
const blue_dino = preload('res://assets/dino/BlueDino.tres')
const red_dino = preload('res://assets/dino/RedDino.tres')
const green_dino = preload('res://assets/dino/GreenDino.tres')
const blue_bullet = preload('res://assets/bullet/blue_bullet.png')
const red_bullet = preload('res://assets/bullet/red_bullet.png')
const green_bullet = preload('res://assets/bullet/green_bullet.png')

var bullet_path = preload('res://scenes/Bullet.tscn')
var velocity = Vector2()
var color = 'b'

onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")

func get_input():
	var animation = "diri_kanan"
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		animation = "jalan_kanan"
		sprite.flip_h = false
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		animation = "jalan_kiri"
		sprite.flip_h = true
	if Input.is_action_just_pressed('shoot'):
		shoot()
	if Input.is_action_just_pressed('change_color'):
		change_color()
	if $Sprite.animation != animation:
		$Sprite.play(animation)
	
	$Node2D.look_at(get_global_mouse_position())


func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func shoot():
	var bullet = bullet_path.instance()
	if color == 'b':
		bullet.get_node('Sprite').set_texture(blue_bullet)
	elif color == 'r':
		bullet.get_node('Sprite').set_texture(red_bullet)
	else:
		bullet.get_node('Sprite').set_texture(green_bullet)
	get_parent().add_child(bullet)
	bullet.position = $Node2D/Position2D.global_position
	bullet.velocity = get_global_mouse_position() - bullet.position

func change_color():
	if color == 'b':
		color = 'r'
		sprite.set_sprite_frames(red_dino)
	elif color == 'r':
		color = 'g'
		sprite.set_sprite_frames(green_dino)
	else:
		color = 'b'
		sprite.set_sprite_frames(blue_dino)
